<?php

	include_once "./class/Articles.php";

	$Articles = new Articles;
	$mainArticle = $Articles->getRandomArticles(1)[0];
	$arSideArticles = $Articles->getRandomArticles(2);
	$arBottomArticles = $Articles->getRandomArticles(3);

?><!doctype html>
<html class="no-js" lang="en">
	<head>
		<meta charset="utf-8">
		<title>Cantaloop Job 2018 Challenge</title>
		<meta name="description" content="Cantaloop Job 2018 Challenge solution.">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/foundation-sites@6.5.0-rc.2/dist/css/foundation.min.css" integrity="sha256-iJQ8dZac/jUYHxiEnZJsyVpKcdq2sQvdA7t02QFmp30= sha384-SplqNBo/0ZlvSdwrP/riIPDozO5ck8+yIm++KVqyMAC53S6m3BaV+2OLpi7ULOOh sha512-ho6hK4sAWdCeqopNZWNy1d9Ok2hzfTLQLcGSr8ZlRzDzh6tNHkVoqSl6wgLsqls3yazwiG9H9dBCtSfPuiLRCQ==" crossorigin="anonymous">
		<link href="https://fonts.googleapis.com/css?family=Roboto:700" rel="stylesheet">
		<link rel="stylesheet" href="css/style.css">
	</head>
	<body>
		
		<div class="firstScreen">
			<div class="bg"></div>
			<img src="img/logo.svg" class="logo">
		</div>
		<div class="firstRow grid-container">
			<div class="grid-x grid-margin-x">
				<div class="medium-7 cell mainArticleCnt">
					<div class="hoverableLink article">
						<div class="respImage">
							<img src="<?= $mainArticle["img"] ?>" alt="<?= $mainArticle["imgAlt"] ?>">
						</div>
						<div class="content">
							<a href="<?= $mainArticle["url"] ?>" class="mainLink"><?= $mainArticle["title"] ?></a>

							<div class="secondaryLinkCnt">
								<a href="http://www.facebook.com" class="secondaryLink">Secondary Link with extra text. efficitur scelerisque velit a pretium. Integer efficitur scelerisque velit a pretium. Integer efficitur scelerisque velit a pretium.</a>
							</div>
						</div>
					</div>
				</div>
				<div class="medium-5 cell sidebar">
					<?php foreach($arSideArticles as $article) : ?>
						<div class="hoverableLink article">
							<div class="grid-x">
								<div class="medium-4 cell">
									<div class="respImage">
										<img src="<?= $article["img"] ?>" alt="<?= $article["imgAlt"] ?>">
									</div>
								</div>
								<div class="medium-8 cell">
									<div class="content">
										<a href="<?= $article["url"] ?>" class="mainLink"><?= $article["title"] ?></a>
									</div>
								</div>
							</div>
						</div>
					<?php endforeach; ?>
				</div>
			</div>
		</div>
		<div class="secondRow grid-container">
			<div class="grid-x grid-margin-x medium-up-3">
				<?php foreach($arBottomArticles as $article) : ?>
					<div class="hoverableLink cell article">
						<div class="respImage">
							<img src="<?= $article["img"] ?>" alt="<?= $article["imgAlt"] ?>">
						</div>
						<div class="content">
							<a href="<?= $article["url"] ?>" class="mainLink"><?= $article["title"] ?></a>
						</div>
					</div>
				<?php endforeach; ?>
			</div>
		</div>


		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
		<script src="js/common.js"></script>

	</body>
</html>