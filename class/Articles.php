<?php

	class Articles {

		private $arArticles = [
			[
				"url" => "http://www.google.com", 
				"title" => "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus congue leo in interdum tristique.", 
				"img" => "img/1.jpg",
				"imgAlt" => "image alt text 1"
			], [
				"url" => "http://www.ntua.gr", 
				"title" => "Phasellus congue leo in interdum tristique.", 
				"img" => "img/2.jpg",
				"imgAlt" => "image alt text 2"
			], [
				"url" => "http://www.cantaloop.gr",
				"title" => "Fusce eu tincidunt ligula. Praesent ultrices massa et tortor tempor, vel interdum ex mattis.", 
				"img" => "img/3.jpg",
				"imgAlt" => "image alt text 3"
			], [
				"url" => "http://www.bing.com",
				"title" => "Fusce eu tincidunt ligula.", 
				"img" => "img/4.jpg",
				"imgAlt" => "image alt text 4"
			], [
				"url" => "http://www.google.com/analytics",
				"title" => "Vivamus vitae erat efficitur, facilisis nisl et, laoreet velit. Etiam id libero a lectus euismod tincidunt.", 
				"img" => "img/5.jpg",
				"imgAlt" => "image alt text 5"
			],
		];

		function getRandomArticles($howMany = 1) {
			shuffle($this->arArticles);
			return array_slice($this->arArticles, 0, $howMany);
		}

	}