$(function(){

	$("body").on("click", ".hoverableLink", function(e){
		var $el = $(this),
			$mainLink = $el.find(".mainLink");


		//skip secondary links
		var $trg = $(e.target);
		if ($trg.hasClass('secondaryLink'))
			return;
		
		if ($mainLink.length > 0) {
			location.href = $mainLink.attr("href");
			return;
		};
		
	});

});